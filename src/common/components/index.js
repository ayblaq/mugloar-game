export { default as Footer } from './Footer/Footer'
export { default as PageLoader } from './PageLoader/PageLoader'
export { default as ErrorPage } from './ErrorPage/ErrorPage'
export {default as ModalTab} from './ModalTab/ModalTab'