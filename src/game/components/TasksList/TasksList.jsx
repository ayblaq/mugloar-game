import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
    Container, Card
} from 'semantic-ui-react'
import './TasksList.css';
import { PageLoader, ErrorPage } from '../../../common/components'
import { TaskCard } from '../../components';
import { updateGameStat } from '../../actions/GameActions'
import { updateTaskInfo, fetchRecordTasks} from '../../actions/TasksAction'
import {solveTaskApi} from '../../api/GameApi'
import { openModal } from '../../../common/actions/ModalActions';

const TasksList = (props) => {

    const {
        tasks: { tasks, isFetching, error, errorMessage },
        game: { game }
    } = useSelector(state => state.game);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchRecordTasks(tasks,game));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    },[game]);

    const checkStatus = (res) => {
        if (res.lives === 0) {
            dispatch(openModal('Game Over. Thank you for participating!. '))
        }
    }
    const solveTask = (taskId) => {

        let update = { ...tasks[taskId], isLoading: true }
        dispatch(updateTaskInfo(update))
        solveTaskApi(game.gameId, taskId)
            .then(response => {
                let cardborder = response.success ? { border: 'success' } : { border: 'error' }
                dispatch(updateTaskInfo({
                    ...update,
                    isLoading: false,
                    action: 'disabled',
                    value: response.message,
                    ...cardborder
                }))
                dispatch(updateGameStat(response))
                checkStatus(response)
            })
            .catch(error => {
                dispatch(updateTaskInfo({ ...update, isLoading: false, action:'disabled',value:"AD EXPIRED" }))
                console.log("Error occurred")
            });
    }

    if (isFetching) return <PageLoader />;
    if (error) return <ErrorPage content={errorMessage} title="OOPS!!! Technical Error" />;

    return (
        <Container>
            <Card.Group>
                {Object.keys(tasks).map((key,i) => <TaskCard gameId={props.gameId} task={tasks[key]} key={i} solveTask={solveTask}></TaskCard> )}
            </Card.Group>
        </Container>
    )
};

export default TasksList;






