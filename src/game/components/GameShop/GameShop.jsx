import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
    Container
} from 'semantic-ui-react'
import './GameShop.scss';
import { PageLoader, ErrorPage } from '../../../common/components'
import { fetchShopItems,updateItem } from '../../actions/ItemsAction'
import ShopItem from '../ShopItem/ShopItem';

const GameShop = (props) => {

    const {
        items: { items, isFetching, error, errorMessage },
        game: {game}
    } = useSelector(state => state.game);
    const dispatch = useDispatch();

    useEffect(() => {
        if(Object.keys(items).length===0)dispatch(fetchShopItems(props.gameId)
        );
       // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const purchaseItem = (item) => {
        dispatch(updateItem(game.gameId,item))
    }

    if (isFetching) return <PageLoader />;
    if (error) {
        return (
            <ErrorPage content={errorMessage} title="OOPS!!! Technical Error" />
        )
    }

    return (
        
        <Container>
            {Object.keys(items).map((key,index)=><ShopItem gameId={props.gameId} item={items[key]} key={index} purchaseItem={purchaseItem}></ShopItem>)}
        </Container>
    )
};

export default GameShop;






