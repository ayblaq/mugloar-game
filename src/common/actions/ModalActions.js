import { fetchGameApi} from "../../game/api/GameApi";
import { startGameSuccess} from "../../game/actions/GameActions";
import {fetchTasksRequest} from "../../game/actions/TasksAction";
import {fetchShopItems} from "../../game/actions/ItemsAction";
export const OPEN_MODAL = 'OPEN_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';

export const closeModal = () => ({
    type: CLOSE_MODAL
})

export const openModal = (msg) => ({
    type: OPEN_MODAL,
    payload: msg
})

export const restartGame = () => (dispatch) => {
    fetchGameApi()
        .then(game => {
            dispatch(fetchTasksRequest())
            dispatch(startGameSuccess(game));
            dispatch(fetchShopItems(game.gameId))
            dispatch(closeModal())
        })
        .catch(error => {
            dispatch(openModal("Could not restart game!"))
        });
};


