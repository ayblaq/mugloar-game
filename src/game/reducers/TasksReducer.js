import {
    FETCH_TASKS_FAILURE,
    FETCH_TASKS_SUCCESS,
    FETCH_TASKS_REQUEST,
    UPDATE_SINGLE_TASK,
    UPDATE_ALL_TASK
} from '../actions/TasksAction'

export const tasksReducer = (state = {
    isFetching: true,
    tasks: {

    },
    errorMessage: '',
    error: false,
}, action) => {
    switch (action.type){
        case FETCH_TASKS_REQUEST:
            return {
                tasks: {},
                isFetching: true,
            };
        case FETCH_TASKS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                tasks: action.payload,

            };
        case FETCH_TASKS_FAILURE:
            return {
                ...state,
                isFetching: false,
                errorMessage: action.payload.message,
                error: true
            }
        case UPDATE_SINGLE_TASK:
            return {
                ...state,
                tasks: {
                    ...state.tasks,
                    [action.payload.task.adId]:{
                        ...action.payload
                    }
                }
            }
        case UPDATE_ALL_TASK:
            return {
                ...state,
                tasks: {
                    ...action.payload
                }
                
            }
        default:
            return state
    }
}