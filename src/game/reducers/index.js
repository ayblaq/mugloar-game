import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { gameReducer } from './GameReducer'
import {tasksReducer} from './TasksReducer'
import {itemsReducer} from './ItemsReducer'
import {modalReducer} from '../../common/reducers/ModalReducer'



const persistConfig = {
  key: 'game',
  storage: storage,
};

const gameReducers = (state = {}, action) => {
  return {
    game: gameReducer(state.game, action),
    tasks: tasksReducer(state.tasks, action),
    modal: modalReducer(state.modal, action),
    items: itemsReducer(state.items, action)
  }
};

const reducer = persistReducer(persistConfig, gameReducers);

export { reducer as gameReducers }
