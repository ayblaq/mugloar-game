const prodConfig = {
    apiUrl: 'https://dragonsofmugloar.com/api/v2/',
    country: 'EST',
    language: 'EN',
};
export default prodConfig;