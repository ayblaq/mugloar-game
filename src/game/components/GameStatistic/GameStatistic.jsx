import React from 'react';
import {
    Icon, Button, Grid
} from 'semantic-ui-react';
import './GameStatistic.scss';

const GameStatistic = (props) => {

    const { statistic, resetList } = props
    const configObj = {
        'lives':{icon:'heart'},
        'gold':{icon:'gem outline'},
        'level':{icon:'arrow up'},
        'score':{icon:'chess knight'},
        'highScore':{icon:'trophy'},
        'turn':{icon:'globe'}
    }
    return (
        <div className="main-container">
            <Grid><Button basic color="green" onClick={resetList}>PULL MORE ADS</Button></Grid>
            <div className="icon-container">
                {Object.keys(statistic).map((key,index) => {
                    if(configObj[key]===undefined)return null;
                    return (<div className="" key={index}>
                        <Icon name={`${configObj[key]?configObj[key].icon:""}`}></Icon>
                        <p>{key.toUpperCase()}<strong className="stat-value">{statistic[key]}</strong></p>
                    </div>)
                })}
            </div>
        </div>
    )
};

export default GameStatistic;






