import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import { gameReducers } from '../game/reducers'

export const createRootReducer = (history) => combineReducers({
  router: connectRouter(history),
  game: gameReducers
});
