import React from 'react'
import './TaskRating.scss';
import {
    Icon
} from 'semantic-ui-react'


const TaskRating = (props) => {

    const probability  = {
        'Risky':2,
        'Sure thing':5,
        'Piece of cake':5,
        'Hmmm....':3,
        'Gamble':2,
        'Walk in the park':5,
        'Rather detrimental':1,
        'Quite likely':3,

    }
    const value = probability[props.name]?probability[props.name]:-1

    return (
        <div className="certainty-container">
            <p>Certainty:</p>
            {[...Array(5)].map((_,i) => {
                return <Icon name="star" key={i} color={`${i<value?'yellow':'grey'}`}></Icon>
            })}
        </div>
    )
};

export default TaskRating;






