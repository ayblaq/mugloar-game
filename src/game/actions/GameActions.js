import { fetchGameApi } from '../api/GameApi';

export const START_GAME_REQUEST = 'START_GAME_REQUEST';
export const START_GAME_SUCCESS = 'START_GAME_SUCCESS';
export const START_GAME_FAILURE = 'START_GAME_FAILURE';
export const UPDATE_GAME_STAT = 'UPDATE_GAME_STAT';

export const startGameRequest = () => ({
    type: START_GAME_REQUEST,
    payload: {
        game: {}
    }
})

export const startGameSuccess = (game) => ({
    type: START_GAME_SUCCESS,
    payload: game
})

export const updateGameStat = (game) => ({
    type: UPDATE_GAME_STAT,
    payload: game
})

export const startGameFailure = (message) => ({
    type: START_GAME_FAILURE,
    payload: new Error(message),
    error: true
})

export const fetchGame = () => (dispatch) => {
    dispatch(startGameRequest());
    fetchGameApi()
        .then(game => {
            dispatch(startGameSuccess(game));
        })
        .catch(error => {
            dispatch(startGameFailure("Technical error connecting game server."))
        });
};