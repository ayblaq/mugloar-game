import { fetchShopItemsApi,buyShopItemApi } from '../api/GameApi';
import {updateGameStat} from '../actions/GameActions';
export const FETCH_ITEMS_REQUEST = 'FETCH_ITEMS_REQUEST';
export const FETCH_ITEMS_SUCCESS = 'FETCH_ITEMS_SUCCESS';
export const FETCH_ITEMS_FAILURE = 'FETCH_ITEMS_FAILURE';
export const UPDATE_SINGLE_ITEM = 'UPDATE_SINGLE_ITEM';

export const fetchItemsSuccess = (items) => ({
    type: FETCH_ITEMS_SUCCESS,
    payload: items
})

export const fetchItemsFailure = (message) => ({
    type: FETCH_ITEMS_FAILURE,
    payload: new Error(message),
    error: true
})

export const fetchItemsRequest = () => ({
    type: FETCH_ITEMS_REQUEST,
    payload: {
        items: {}
    }
})

export const updateItemInfo = (item) => ({
    type: UPDATE_SINGLE_ITEM,
    payload: item
})

export const fetchShopItems = (id) => (dispatch) => {
    dispatch(fetchItemsRequest());
    fetchShopItemsApi(id)
        .then(items => {
            let res = items.reduce((obj, item) => {
                obj[item['id']] = {
                    item: { ...item },
                    isLoading: false,
                    value: 'BUY',
                    action: 'active',
                    message: ''
                }
                return obj;
            }, {})
            dispatch(fetchItemsSuccess(res));
        })
        .catch(error => {
            dispatch(fetchItemsFailure("Technical error making request to get shop items"))
        });
};

export const updateItem = (gameId, item) => (dispatch) => {
    if (item.value === "Purchased") return;
    item = { ...item, isLoading: true }
    dispatch(updateItemInfo(item))
    buyShopItemApi(gameId, item.item.id)
        .then(response => {
            dispatch(updateGameStat(response))
            item = response.shoppingSuccess ? { ...item, isLoading: false, action: 'disabled', value: 'PURCHASED', message: '' }
                : { ...item, isLoading: false, action: 'active', message: 'Last purchase NOT successful.' };
            dispatch(updateItemInfo(item))
        })
        .catch(error => {
            item = { ...item, isLoading: false, message: 'Error occured during purchase' }
            dispatch(updateItemInfo(item))
        });
}