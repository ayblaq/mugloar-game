import React from 'react'
import {
    Container, List, Icon
} from 'semantic-ui-react'
import './GameRules.css';

const GameRules = (props) => {

    return (
        <Container>
            <List>
                <List.Item>
                    <Icon name="audio description" color="grey" size="big"></Icon>
                    <List.Content>
                        <List.Header as='a'>AD CARDS</List.Header>
                        <List.Description>Cards can be <strong>picked</strong> and <strong>solved.</strong></List.Description>
                    </List.Content>
                </List.Item>
                <List.Item>
                    <Icon name="time" size="big" color="grey"></Icon>
                    <List.Content>
                        <List.Header as='a'>EXPIRES IN</List.Header>
                        <List.Description>This indicates that an AD card will <strong>expire</strong> after specified number of <strong>turn</strong> is exceeded.</List.Description>
                    </List.Content>
                </List.Item>
                <List.Item>
                    <Icon name="heart outline" size="big" color="red"></Icon>
                    <List.Content>
                        <List.Header as='a'>3 LIVES</List.Header>
                        <List.Description>You are allowed to pick a <strong>wrong</strong> card <strong>3 times </strong> and after this, <strong>game over</strong>.</List.Description>
                    </List.Content>
                </List.Item>
                <List.Item>
                    <Icon name="audio description" color="green" size="big"></Icon>
                    <List.Content>
                        <List.Header as='a'>GREEN CARDS</List.Header>
                        <List.Description>This indicates that the selection was <strong>successful</strong>.</List.Description>
                    </List.Content>
                </List.Item>
                <List.Item>
                    <Icon name="audio description" color="red" size="big"></Icon>
                    <List.Content>
                        <List.Header as='a'>RED CARDS</List.Header>
                        <List.Description>This indicates that the selection was <strong>not successful</strong>.</List.Description>
                    </List.Content>
                </List.Item>
                <List.Item>
                    <Icon name="gem outline" size="big"></Icon>
                    <List.Content>
                        <List.Header as='a'>REWARDS</List.Header>
                        <List.Description> Rewards are given after <strong>succesful</strong> selection and the <strong> expected value</strong> can be found on the card.</List.Description>
                    </List.Content>
                </List.Item>
                <List.Item>
                    <Icon name="smile" color="green" size="big"></Icon>
                    <List.Content>
                        <List.Header as='a'>CHEAT</List.Header>
                        <List.Description>Always look out for <strong>Walk in the park</strong>, <strong>Piece of cake </strong> and <strong>Sure thing</strong></List.Description>
                    </List.Content>
                </List.Item>
            </List>
        </Container>
    )
};

export default GameRules






