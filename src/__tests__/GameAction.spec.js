import React from "react";
import { create } from "react-test-renderer";
import * as actions from '../game/actions/GameActions';
import * as reducers from '../game/reducers/GameReducer';

describe("Start game action", () => {
  test("Should create a start new game request", () => {
    const expectedAction = {
        type: actions.START_GAME_REQUEST,
        payload: {
            game: {}
        }
    }
    expect(actions.startGameRequest()).toEqual(expectedAction);
  });
});

describe("Start Game Reducer", () => {
    test("It should return my initial state", () => {
        expect(reducers.gameReducer(undefined, {})).toEqual(
            {    
            isFetching: true,
            game: {},
            errorMessage: '',
            error: false}
            )
    })
})
