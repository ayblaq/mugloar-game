import {
    START_GAME_REQUEST,
    START_GAME_SUCCESS,
    START_GAME_FAILURE,
    UPDATE_GAME_STAT
} from '../actions/GameActions'

export const gameReducer = (state = {
    isFetching: true,
    game: {},
    errorMessage: '',
    error: false
}, action) => {
    switch (action.type) {
        case START_GAME_REQUEST:
            return {
                ...state,
                isFetching: true,
                error: false
            };
        case START_GAME_SUCCESS:
            return {
                ...state,
                isFetching: false,
                game: {
                    ...action.payload,
                    oldturn: 0
                }
            };
        case UPDATE_GAME_STAT:
            return {
                ...state,
                game:{
                    ...state.game,
                    oldturn: state.game.turn,
                    ...action.payload
                }
            };
        case START_GAME_FAILURE:
            return {
                ...state,
                isFetching: false,
                errorMessage: action.payload.message,
                error: true
            }
        default:
            return state
    }
}