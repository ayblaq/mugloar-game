import React from 'react'
import {
    Card, Button
} from 'semantic-ui-react'
import './ShopItem.scss';

const ShopItem = (props) => {

    const { item,purchaseItem } = props
    
    return (
        <Card>
            <Card.Content>
                <Card.Header className="small-header">
                    <p className="title">{item.item.name}</p>
                    <p className="price"><strong>{item.item.cost} GOLDS</strong></p>
                </Card.Header>
                <Card.Meta>{item.message}</Card.Meta>
            </Card.Content>
            <Card.Content extra>
                <Button className={`${item.action}`} loading={item.isLoading} onClick={() => purchaseItem(item)} size="tiny">{item.value}</Button>
            </Card.Content>
        </Card>
    )
};

export default ShopItem;






