export { default as TasksList } from './TasksList/TasksList'
export { default as GameStatistic } from './GameStatistic/GameStatistic'
export { default as TaskCard } from './TaskCard/TaskCard'
export { default as TaskRating } from './TaskRating/TaskRating'
export { default as GameRules } from './GameRules/GameRules'
export { default as GameShop } from './GameShop/GameShop'