import {axiosClient } from '../../api';

export async function fetchGameApi() {
    return await axiosClient.post('/game/start')
    .then((response) => {
        return response.data;
    })
    .catch(error => { throw error});
}

export async function fetchTasksApi(id) {
    return await axiosClient.get(`/${id}/messages`)
    .then((response) => {
        return response.data;
    })
    .catch(error => { throw error});
}

export async function solveTaskApi(gameId,taskId){
    return await axiosClient.post(`/${gameId}/solve/${taskId}`)
    .then((response) => {
        return response.data;
    })
    .catch(error => { throw error});
}

export async function fetchShopItemsApi(gameId) {
    return await axiosClient.get(`/${gameId}/shop`)
    .then((response) => {
        return response.data;
    })
    .catch(error => { throw error});
}

export async function buyShopItemApi(gameId,itemId) {
    return await axiosClient.post(`/${gameId}/shop/buy/${itemId}`)
    .then((response) => {
        return response.data;
    })
    .catch(error => { throw error});
}
