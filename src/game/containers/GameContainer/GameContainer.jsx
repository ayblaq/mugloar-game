import React from 'react'
import { useSelector } from 'react-redux'
import {
  Container,
  Grid, Tab
} from 'semantic-ui-react'
import { TasksList, GameStatistic } from '../../components/'
import './GameContainer.scss'
import { Home } from '../../../home/components'
import { fetchTasks } from '../../actions/TasksAction'
import { useDispatch } from 'react-redux'
import { ModalTab } from '../../../common/components'
import {GameShop,GameRules} from '../../components'

const GameContainer = () => {

  const {
    game: { game }
  } = useSelector(state => state.game)
  const dispatch = useDispatch()

  if (!('gameId' in game)) {
    return (
      <Home></Home>
    )
  }
  const resetList = () => {
    dispatch(fetchTasks(game.gameId))
  }
  const panes = [
    { menuItem: 'HELP', render: () => <Tab.Pane><GameRules></GameRules></Tab.Pane>},
    { menuItem: 'SHOP', render: () => <Tab.Pane className="shop-view"><GameShop gameId={game.gameId}></GameShop></Tab.Pane>},
  ]

  return (
    <Container>
      <Grid className="game-container" stackable columns={2} centered divided >
        <Grid.Row className="sticky-header">
          <GameStatistic statistic={game} resetList={resetList}></GameStatistic>
        </Grid.Row>
        <Grid.Row className="main-board">
          <Grid.Column width={11} className="tasks-list">
            <TasksList gameId={game.gameId}></TasksList>
          </Grid.Column>
          <Grid.Column width={4}>
            <Tab panes={panes} className="pane-tab"></Tab>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <ModalTab></ModalTab>
    </Container>
  )
};

export default GameContainer;






