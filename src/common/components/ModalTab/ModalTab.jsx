import React from 'react'
import { Button, Modal } from 'semantic-ui-react'
import { useDispatch, useSelector } from 'react-redux';
import { restartGame,closeModal } from '../../../common/actions/ModalActions'
const ModalTab = () => {

    const {
        modal: { open, message },
        game: { game,isFetching }
    } = useSelector(state => state.game)
    const dispatch = useDispatch()
    const reset = () => {
        dispatch(restartGame())
    }
    const close = () => {
        dispatch(closeModal())
    }

    return (
        <Modal open={open} closeOnEscape={false} closeOnDimmerClick={false} onClose={reset}>
            <Modal.Header>Message</Modal.Header>
            <Modal.Content>
                <p>{message}</p>
            </Modal.Content>
            <Modal.Actions>
                {game.lives > 0 && isFetching===false?<Button onClick={close} negative>CLOSE WINDOW</Button>:null}
                <Button
                    onClick={reset}
                    positive
                    labelPosition='right'
                    icon='checkmark'
                    content='RESTART GAME'
                />
            </Modal.Actions>
        </Modal>
    )
}

export default ModalTab;