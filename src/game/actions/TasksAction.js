
import { fetchTasksApi } from '../api/GameApi';
import Base64 from 'crypto-js/enc-base64';
import Utf8 from 'crypto-js/enc-utf8'
export const FETCH_TASKS_REQUEST = 'FETCH_TASKS_REQUEST';
export const FETCH_TASKS_SUCCESS = 'FETCH_TASKS_SUCCESS';
export const FETCH_TASKS_FAILURE = 'FETCH_TASKS_FAILURE';
export const UPDATE_SINGLE_TASK = 'UPDATE_SINGLE_TASK';
export const UPDATE_ALL_TASK = 'UPDATE_ALL_TASK';

export const fetchTasksRequest = () => ({
    type: FETCH_TASKS_REQUEST,
    payload: {
        tasks: {}
    }
})

export const fetchTasksSuccess = (tasks) => ({
    type: FETCH_TASKS_SUCCESS,
    payload: tasks
})

export const fetchTasksFailure = (message) => ({
    type: FETCH_TASKS_FAILURE,
    payload: new Error(message),
    error: true
})

export const updateTaskInfo = (task) => ({
    type: UPDATE_SINGLE_TASK,
    payload: task
})

export const updateAllTask = (tasks) => ({
    type: UPDATE_ALL_TASK,
    payload: tasks
})

const decryptWord = (word) => {
    try {
        word = Base64.parse(`${word}`)
        word = Utf8.stringify(word)
        return word;
    } catch (e) {
        return word;
    }
}

const checkEncoding = (task) => {
    if (task.encrypted !== undefined && task.encrypted === 1) {
        task.probability = decryptWord(task.probability)
        task.message = decryptWord(task.message)
        task.adId = decryptWord(task.adId)
    }

    return task;
}

export const fetchRecordTasks = (tasks, game) => (dispatch) => {
    if (Object.keys(tasks).length === 0) {
        dispatch(fetchTasks(game.gameId))
    } else {
        let res = Object.entries(tasks).reduce((newObj, [key, val]) => {
            newObj[key] = {
                ...val,
                task: {
                    ...val.task,
                    isLoading: false,
                    expiresIn: val.task.expiresIn > 0 && val.border === "normal" ? val.task.expiresIn - 1 : val.task.expiresIn,
                },
                action: val.task.expiresIn === 1 ? 'disabled' : val.action,
                value: val.task.expiresIn === 1 && val.border === "normal" ? 'AD EXPIRED' : val.value
            }
            return newObj;
        }, {})
        dispatch(updateAllTask(res))
    }
}

export const fetchTasks = (id) => (dispatch) => {
    dispatch(fetchTasksRequest());
    fetchTasksApi(id)
        .then(tasks => {
            let res = tasks.reduce((obj, task) => {
                task = checkEncoding(task)
                obj[task['adId']] = {
                    task: { ...task },
                    isLoading: false,
                    value: 'PICK',
                    action: 'active',
                    border: 'normal',
                    suggestion: ''
                }
                return obj;
            }, {})
            dispatch(fetchTasksSuccess(res));
        })
        .catch(error => {
            dispatch(fetchTasksFailure("Technical error making request to get game tasks"))
        });
};
