import {
    FETCH_ITEMS_FAILURE,
    FETCH_ITEMS_SUCCESS,
    FETCH_ITEMS_REQUEST,
    UPDATE_SINGLE_ITEM
} from '../actions/ItemsAction'

export const itemsReducer = (state = {
    isFetching: false,
    items: {
    },
    errorMessage: '',
    error: false,
}, action) => {
    switch (action.type){
        case FETCH_ITEMS_REQUEST:
            return {
                isFetching: true,
                items:{}
            };
        case FETCH_ITEMS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                items: action.payload
            };
        case FETCH_ITEMS_FAILURE:
            return {
                ...state,
                isFetching: false,
                errorMessage: action.payload.message,
                error: true
            }
        case UPDATE_SINGLE_ITEM:
            return {
                ...state,
                items: {
                    ...state.items,
                    [action.payload.item.id]:{
                        ...action.payload
                    }
                }

            }
        default:
            return state
    }
}