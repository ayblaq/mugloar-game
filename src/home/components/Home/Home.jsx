import React, { useState, useEffect } from 'react'
import {
    Container,
    Button
} from 'semantic-ui-react'
import dragon from '../../../images/dragon-ball.png';
import './Home.css';
import { history } from './../../../routing'
import { useSelector, useDispatch } from 'react-redux'
import { fetchGame } from '../../../game/actions/GameActions'



const Home = () => {
    const [load, setLoading] = useState({ isLoading: false })

    const {
        game: { isFetching, error}
    } = useSelector(state => state.game)

    useEffect(() => {
        if(isFetching===false && error===false){
            history.push('/playgame')
        }
    });

    const dispatch = useDispatch()
    
    let navigate = (e) => {
        setLoading({ isLoading: true })
        dispatch(fetchGame())
    }

    return (
        <Container fluid>
            <header className="App-header">
                <img src={dragon} className="App-logo" alt="logo" />
                <p>DRAGONS OF MUGLOAR</p>
                <Button size="big" disabled={load.isLoading} onClick={navigate}>{load.isLoading ? "LOADING GAME..." : "START GAME"}</Button>
            </header>
        </Container>
    )
};

export default Home;






