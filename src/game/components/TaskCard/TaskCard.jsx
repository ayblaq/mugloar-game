import React from 'react'
import {
    Card, Button
} from 'semantic-ui-react'
import './TaskCard.scss';
import { TaskRating } from '../../components'

const TaskCard = (props) => {

    const { solveTask } = props
    const { task, value, action,isLoading, border} = props.task
    return (
        <Card className={`${border}`}>
            <Card.Content>
                <Card.Header>{task.probability}</Card.Header>
                <Card.Meta>Expected Reward <strong>{task.reward}</strong></Card.Meta>
                <Card.Meta>Expires in <strong>{task.expiresIn} turns</strong></Card.Meta>
                <Card.Meta><TaskRating name={task.probability}></TaskRating></Card.Meta>
                <Card.Description>{task.message}</Card.Description>
            </Card.Content>
            <Card.Content extra>
                <div className='ui two buttons'>
                    <Button basic onClick={() => solveTask(task.adId)} loading={isLoading} className={` card-button ${action}`}>{value}</Button>
                </div>
            </Card.Content>
        </Card>
    )
};

export default TaskCard;






