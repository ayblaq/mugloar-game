import {
    OPEN_MODAL,
    CLOSE_MODAL
} from '../actions/ModalActions'

export const modalReducer = (state = {
    open: false,
    message: "An Alert!"
}, action) => {
    switch (action.type){
        case OPEN_MODAL:
            return {
                ...state,
                open: true,
                message: action.payload
            }
        case CLOSE_MODAL:
            return {
                ...state,
                open: false
            }
        default:
            return state
    }
}