import React from 'react'
import { Message, Container } from 'semantic-ui-react'
import './ErrorPage.scss'


const ErrorPage = (props) => {
    const { title, content } = props;
    return (
        <Container fluid={false}>
            <Message icon='cancel' header={title} content={content} negative/>
        </Container>
    )
};

export default ErrorPage
